# Mittelhochdeutsch-Korpus

Rund 1 100 000 Wortformen aus gut 100 zeitlich und räumlich gestreuten, verfassernah überlieferten Texten – handschriftentreu transkribiert – lexikalisch, morphologisch, morphosyntaktisch und graphonologisch annotiert. Erarbeitet vor allem im Rahmen eines DFG-Vorhabens zur Erstellung einer mhd. Grammatik: <a href="https://d-nb.info/996588450">Klein / Solms / Wegera</a>.

## Lizenz

<a href="https://creativecommons.org/licenses/by-sa/4.0/legalcode.de">CC BY-SA</a>

## Auswahl und Zitation von Zweigen

Das hier, <https://daten.badw.de/mhd-korpus>, veröffentlichte Korpus geht unmittelbar auf das genannte DFG-Vorhaben zurück, enthält alle dort erarbeiteten Annotationen und wird durch seitdem hinzukommende Verbesserungen auf neuestem Stand gehalten.

Die Veröffentlichung gliedert sich in auflagenartige Fassungen: Die zunächst ausgewählte “arbeitsfassung” ist die in Arbeit befindliche, also veränderliche Fassung. Von ihr abgezweigt und oben über den zunächst mit “arbeitsfassung” beschriebenen Auswahlknopf erreichbar sind Fassungen wie “auflage_2020”, die nicht mehr verändert werden und daher als Verweisziele geeignet sind. Es empfiehlt sich, sie eindeutigkeitshalber mit ihrem jeweiligen Auflagendatum und steten Link anzuführen, z. B. “Müller, Stefan / Klein, Thomas: Mittelhochdeutsch-Korpus. Erarbeitet vom DFG-Projekt Mittelhochdeutsche Grammatik: <https://daten.badw.de/mhd-korpus/-/blob/auflage_2020/Mitarbeiter.md>. (2020; <https://daten.badw.de/mhd-korpus/-/tree/auflage_2020>)”.

## Herunterladen der Urdaten und Leseansichten

Die Daten im Ordner “P” sind die Urdaten und zumal für **skriptgestützte Auswertungen** gedacht. Diese Daten werden hier weniger zur Ansicht als zum Herunterladen vorgehalten: Dafür klicke man sich in den genannten Ordner “P”, dann auf den Knopf mit dem Abspeichern-Zeichen (oben zwischen den Knöpfen “Lock”, “History”, “Find file”, “Web IDE” einerseits und dem blauen Knopf “Clone” andererseits) und dann unter “Download this directory” auf “zip”. Entsprechend kann man hier auch ein anderes Format als “zip” auswählen oder sich weiter in Unterverzeichnisse vorklicken und nur diese herunterladen. – Um diese Urdaten einsehen zu können, ohne sie herunterzuladen zu müssen, klicke man sich in die Datei “Leseansichten_roh.md” und darin auf den gewünschten Text.

Die Daten im Ordner “P-htm” sind für **Leseansichten** gedacht: Dafür klicke man sich in die Datei “Leseansichten.md” und darin auf den gewünschten Text.

## Entstehung, Gestalt und Inhalt

Die Urdaten sind im Zeichenschlüssel <a href="https://de.wikipedia.org/wiki/Codepage_437">Codepage 437</a> gespeichert.

**Zum Verständnis der Daten siehe die <a href="https://dienst.badw.de/varia?form=pdf&url=https%3A%2F%2Fdaten.badw.de%2Fmhd-korpus%2F-%2Fraw%2Farbeitsfassung%2FK%25C3%25BCrzel%25C3%25BCbersicht.pdf?inline=false">Kürzelübersicht</a> und das <a href="https://dienst.badw.de/varia?form=pdf&url=https%3A%2F%2Fdaten.badw.de%2Fmhd-korpus%2F-%2Fraw%2Farbeitsfassung%2FKorpushandbuch.pdf?inline=false">Korpushandbuch</a>.**

## Weitere Auskunft

“mueller” ad “badw.de”; “thomas.klein” ad “uni-bonn.de”
