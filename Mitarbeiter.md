# Leiter und Mitarbeiter am Mittelhochdeutsch-Korpus

## Arbeitsstelle Bochum (Transkription)

- (Leitung:) **Wegera, Klaus-Peter**
- Florath, Alexandra
- Kaspers, Sabine
- Tersteegen, Helge
- Tersteegen, Susanne
- Walter, Stefanie

## Arbeitsstelle Bonn (Transkription, Präeditierung, Annotation, Korpusbeschreibung, Regelwerk, Programmierung)

- (Leitung:) **Klein, Thomas** (u. a. Korpusbeschreibung, Regelwerk, Programmierung – im Wesentlichen oder in weiten Teilen)
- Büthe-Scheider, Eva
- Chlench, Kathrin (u. a. Korpusbeschreibung)
- Cuno-Janßen, Laila
- Eschke, Lars
- Fofulit, Oksana
- Fuß, Martin
- Fußer, Marika
- Gasparyan, Gohar
- Hahn (Ritter), Susanne
- Hentschel, Christine
- Holt, Ian
- Kemper, Tobias Anselm (u. a. Korpusbeschreibung)
- Kläs, Claudia
- Krause, Bernd
- Lennertz, Martina
- Lenz-Kemper, Barbara (u. a. Korpusbeschreibung)
- Lingscheid, Claudia
- Meyer, Diane
- Micklin (Joschko), Anja
- Mischke, Astrid
- Müller, Stefan (u. a. Regelwerk, Programmierung)
- Neunzig, Anke
- Ringeler, Frank
- Strubel-Burgdorf, Susanne
- Weber, Elke
- Weimann (Huhn), Britta
- Wevers, Hendrik
- Wojtinnek, Pia-Ramona
- Wolf, Wiebke

## Arbeitsstelle Halle (Transkription)

- (Leitung:) **Solms, Hans-Joachim**
- Klukas, Dany-Sophie
- Leipold, Aletta
- Link, Heike
- Mindl-Mohr, Antje
- Rönsch, Denise
- Weinert, Jörn
